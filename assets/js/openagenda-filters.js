/**
 * @file
 * Contains the definition of the OpenAgenda filters behaviour.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.openAgendaFilters = {
    attach: function (context) {
      // Load only once.
      if (context !== document) {
        return;
      }

      // window.oa.
      if (typeof window.oa === 'undefined') {
        window.oa = {
          query: window.location.search,
          locale: drupalSettings.openagenda.lang,
          locales: {
            en: {
              eventsTotal: '{total, plural, =0 {No events match this search} one {{total} event} other {{total} events}}'
            },
            fr: {
              eventsTotal: '{total, plural, =0 {Aucun événement ne correspond à cette recherche} one {{total} événement} other {{total} événements}}'
            }
          },
          res: drupalSettings.openagenda.filtersUrl,
          ajaxUrl: drupalSettings.openagenda.ajaxUrl,
          filtersRef: null,
          values: null,
          queryParams: null,
          queryParams: null,
          manualSubmit: drupalSettings.openagenda.manualSubmit,
          onLoad: async (values, aggregations, filtersRef, _form) => {
            oa.filtersRef = filtersRef;
            oa.values = values;
            oa.queryParams = { ...values, aggregations };

            const formElems = document.querySelectorAll('.oa-filter-form-submit');
            formElems.forEach(function (element) {
              element.addEventListener('submit', (event) => {
                event.preventDefault();
                _form.submit();
              });
            });

            // Update filters.
            await oa.updateFiltersAndWidgets();
          },
          onFilterChange: async (values, aggregations, filtersRef, _form) => {
            oa.filtersRef = filtersRef;
            oa.values = values;
            oa.queryParams = { ...values, aggregations };

            // Show Ajax Throbber, automatically removed when content is replaced/page reloaded.
            $('#oa-wrapper').append(Drupal.theme.ajaxProgressIndicatorFullscreen());

            // Load events.
            Drupal.ajax({
              url: oa.ajaxUrl + '?' + $.param(oa.queryParams)
            }).execute();

            // Update filters and location.
            oa.updateFiltersAndWidgets();
            oa.filtersRef.updateLocation(oa.values);
          },
          updateFiltersAndWidgets: async () => {

            $.ajax({
              url: oa.res + '?' + $.param(oa.queryParams),
              type: 'GET',
              dataType: 'json',
              async: true,
              complete: (data) => {
                oa.filtersRef.updateFiltersAndWidgets(oa.values, data.responseJSON);
              }
            });
          },
        };

        // Load here the library react filter to initialize filers
        // To be sure that the library loads after
        const js = document.createElement('script');
        const fjs = document.getElementsByTagName("script")[0];
        js.src = 'https://unpkg.com/@openagenda/react-filters@2.12.6/dist/main.js';
        js.async = true;
        fjs.parentNode.insertBefore(js, fjs);
      }


    }
  };
})(jQuery, Drupal, drupalSettings);
