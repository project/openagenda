<?php

namespace Drupal\openagenda\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\openagenda\OpenagendaHelperInterface;
use OpenAgendaSdk\OpenAgendaSdk;
use Symfony\Component\DependencyInjection\ContainerInterface;
use \Drupal;

/**
 * Provides the OpenAgenda additional field filter Block.
 *
 * @Block(
 *   id = "openagenda_additional_field_filter_block",
 *   admin_label = @Translation("OpenAgenda - Additional field filter"),
 *   category = @Translation("OpenAgenda"),
 *   context_definitions = {
 *     "node" = @ContextDefinition("entity:node", label = @Translation("Node"))
 *   },
 * )
 */
class OpenagendaAdditionalFieldFilterBlock extends BlockBase implements ContainerFactoryPluginInterface
{

  /**
   * The OpenAgenda helper service.
   *
   * @var \Drupal\openagenda\OpenagendaHelperInterface
   */
  protected $helper;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * OpenAgenda SDK.
   *
   * @var OpenAgendaSdk
   */
  protected $sdk;

  /**
   * OpenAgenda module configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $moduleConfig;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route_match, OpenagendaHelperInterface $helper, ConfigFactoryInterface $config_factory)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $route_match;
    $this->helper = $helper;
    $this->moduleConfig = $config_factory->get('openagenda.settings');

    $this->sdk = new OpenAgendaSdk($this->moduleConfig->get('openagenda.public_key', ''));
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('openagenda.helper'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account)
  {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $node = $this->getContextValue('node');
    $block = [];
    $config = $this->getConfiguration();

    // Check that we have an OpenAgenda node and that we are hitting the base
    // route (not an event).
    if ($node && $node->hasField('field_openagenda') && $this->routeMatch->getRouteName() == 'entity.node.canonical') {
      $preFilters = $this->helper->getPreFilters($node);
      $defaultPlaceholder = $this->moduleConfig->get('openagenda.default_search_filter_placeholder');
      $placeholder = !empty($this->configuration['input_placeholder']) ? $this->configuration['input_placeholder'] : $this->t($defaultPlaceholder);
      $aria_label = !empty($this->configuration['input_aria_label']) ? $this->configuration['input_aria_label'] : $this->t($defaultPlaceholder);

      // Only display in preFilters doesn't contain an thematique entry.
      if (!isset($preFilters['thematique'])) {
        $block = [
          '#theme' => 'block__openagenda_additional_field_filter',
          '#additional_field' => $config['additional_field'],
          '#placeholder' => $placeholder,
          '#aria_label' => $aria_label,
        ];
      }
    }

    return $block;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state)
  {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['additional_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Additional field'),
      '#description' => $this->t('Agenda additional field name'),
      '#default_value' => $config['additional_field'] ?? '',
      '#required' => TRUE,
    ];
    $form['input_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#description' => $this->t('Placeholder for the input field.'),
      '#default_value' => isset($config['input_placeholder']) ? $config['input_placeholder'] : $this->moduleConfig->get('openagenda.default_search_filter_placeholder'),
    ];
    $form['input_aria_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Aria label'),
      '#description' => $this->t('Aria label for the input field.'),
      '#default_value' => isset($config['input_aria_label']) ? $config['input_aria_label'] : $this->moduleConfig->get('openagenda.default_search_filter_placeholder'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state)
  {
    $customField = $form_state->getValue('additional_field');
    $exists = FALSE;

    foreach ($this->sdk->getMyAgendasUids() as $uid) {
      $agendaCustomFields = $this->sdk->getAgendaAdditionalFields($uid);
      foreach ($agendaCustomFields as $agendaCustomField) {
        if ($agendaCustomField == $customField) {
          $exists = TRUE;

          break 2;
        }
      }
    }

    if (!$exists) {
      $form_state->setErrorByName('additional_field', $this->t("This additional field doesn't belong to any of your agenda(s)!"));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state)
  {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['additional_field'] = $values['additional_field'];
    $this->configuration['input_placeholder'] = $values['input_placeholder'];
    $this->configuration['input_aria_label'] = $values['input_aria_label'];
  }

  /**
   * @return int
   *   Cache max age.
   */
  public function getCacheMaxAge()
  {
    return 0;
  }

}
