<?php

namespace Drupal\openagenda\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\openagenda\OpenagendaHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use \Drupal;

/**
 * Provides the OpenAgenda geographic filter Block.
 *
 * @Block(
 *   id = "openagenda_geographic_filter_block",
 *   admin_label = @Translation("OpenAgenda - Geographic filter"),
 *   category = @Translation("OpenAgenda"),
 *   context_definitions = {
 *     "node" = @ContextDefinition("entity:node", label = @Translation("Node"))
 *   },
 * )
 */
class OpenagendaGeographicFilterBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The OpenAgenda helper service.
   *
   * @var \Drupal\openagenda\OpenagendaHelperInterface
   */
  protected $helper;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * OpenAgenda module configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $moduleConfig;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route_match, OpenagendaHelperInterface $helper, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $route_match;
    $this->helper = $helper;
    $this->moduleConfig = $config_factory->get('openagenda.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('openagenda.helper'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $node = $this->getContextValue('node');
    $block = [];
    $defaultPlaceholder = $this->moduleConfig->get('openagenda.default_choice_filter_placeholder');
    $placeholder = !empty($this->configuration['input_placeholder']) ? $this->configuration['input_placeholder'] : $this->t($defaultPlaceholder);
    $aria_label = !empty($this->configuration['input_aria_label']) ? $this->configuration['input_aria_label'] : $this->t($defaultPlaceholder);
    $filter_type = $this->configuration['filter_type'];

    // Check that we have an OpenAgenda node and that we are hitting the base
    // route (not an event).
    if ($node && $node->hasField('field_openagenda') && $this->routeMatch->getRouteName() == 'entity.node.canonical') {
      $block = [
        '#theme' => 'block__openagenda_geographic_filter',
        '#placeholder' => $placeholder,
        '#aria_label' => $aria_label,
        '#filter_type' => $filter_type
      ];
    }

    return $block;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state)
  {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['input_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#description' => $this->t('Placeholder for the input field.'),
      '#default_value' => isset($config['input_placeholder']) ? $config['input_placeholder'] : $this->moduleConfig->get('openagenda.default_choice_filter_placeholder'),
    ];
    $form['input_aria_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Aria label'),
      '#description' => $this->t('Aria label for the input field.'),
      '#default_value' => isset($config['input_aria_label']) ? $config['input_aria_label'] : $this->moduleConfig->get('openagenda.default_choice_filter_placeholder'),
    ];
    $form['filter_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Filter type'),
      '#description' => $this->t('Filter type based on geographic subdivision.'),
      '#options' => [
        'region' => $this->t('Region'),
        'department' => $this->t('Department'),
        'city' => $this->t('City'),
        'adminLevel3' => $this->t('Inter-city'),
        'district' => $this->t('Districts'),
        'locationUid' => $this->t('Location'),
      ],
      '#default_value' => isset($config['filter_type']) ? $config['filter_type'] : NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state)
  {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['input_placeholder'] = $values['input_placeholder'];
    $this->configuration['input_aria_label'] = $values['input_aria_label'];
    $this->configuration['filter_type'] = $values['filter_type'];
  }

  /**
   * @return int
   *   Cache max age.
   */
  public function getCacheMaxAge()
  {
    return 0;
  }

}
